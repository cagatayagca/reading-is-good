
# Reading Is Good App

An application for retail book store.


## Tech stack

* JDK 11
* H2 database
* Lombok
* Junit5
* Springboot version 2.6.3
* Spring Security
* Swagger ui version 3.0.0
* Liquibase

## API Reference

#### Create New Book

```http
  POST /books
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `createBookRequest` | `CreateBookRequest` | **Required**. name, author, quantity and amount info for the book. |

For creating new book into the repository.

#### Add new customer

```http
  POST /customers
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `customerRequest`      | `CustomerRequest` | **Required**. customer info for inserting new one. |

To add new customer to the database.

#### Get customer orders

```http
  POST /customers/{id}/orders
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `Long` | **Required**. customer id for order query. |

#### Get order by id

```http
  GET /orders/{id}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `Long` | **Required**. order id to query. |



Takes order id and returns details.

#### POST new order

```http
  POST /orders
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `orderRequest`      | `OrderRequest` | **Required**. order details; customer id and book id for ordering. |

Takes book and customer id to place a new order.


#### GET stats

```http
  GET /statistics/{customerId}/monthly-report
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `customerId`      | `Long` | **Required**. customer id for querying stats. |

Returns total amount, quantity and order count that is ordered for customer.


#### TEST
 ````http
GET /test
 ````

for ping-pong

## Postman

* ```reading-is-good.postman_collection.json``` collection.

## How To Install

```sh
mvn clean install
```
```sh
docker build - < Dockerfile
docker run -p 8080:8080 
```

## Author

👤 *Lutfu Cagatay Agca*