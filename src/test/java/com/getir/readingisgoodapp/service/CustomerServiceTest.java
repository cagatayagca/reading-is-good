package com.getir.readingisgoodapp.service;

import com.getir.readingisgoodapp.exception.CustomerNotFoundException;
import com.getir.readingisgoodapp.model.Book;
import com.getir.readingisgoodapp.model.Customer;
import com.getir.readingisgoodapp.model.OrderHeader;
import com.getir.readingisgoodapp.model.OrderRecords;
import com.getir.readingisgoodapp.model.request.CustomerRequest;
import com.getir.readingisgoodapp.model.response.CustomerResponse;
import com.getir.readingisgoodapp.model.response.OrderResponse;
import com.getir.readingisgoodapp.repo.CustomerRepo;
import com.getir.readingisgoodapp.repo.OrderHeaderRepo;
import com.getir.readingisgoodapp.service.impl.CustomerServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CustomerServiceTest {

    @InjectMocks
    @Spy
    private CustomerServiceImpl customerService;

    @Mock
    private CustomerRepo customerRepo;

    @Mock
    private OrderHeaderRepo orderHeaderRepo;
    private static CustomerRequest customerRequest;

    @BeforeEach
    public void init() {
        customerRequest = CustomerRequest.builder()
                .name("Test")
                .build();
    }

    @Test()
    public void createCustomerTest_whenCustomerRequestComesIn_thenReturnSuccessCustomerResponse() {
        Customer customer = Customer.builder()
                .name("Test")
                .id(1L)
                .build();
        when(customerRepo.save(any(Customer.class)))
                .thenReturn(customer);
        CustomerResponse customerResponse = customerService.createCustomer(customerRequest);
        assertThat(customerResponse.getId()).isEqualTo(1L);
        assertThat(customerResponse.getName()).isEqualTo("Test");
    }

    @Test()
    public void getOrdersFromCustomerTest_whenCustomerNotFound_thenThrowCustomerNotFoundException() {
        when(customerRepo.findById(1L))
                .thenThrow(new CustomerNotFoundException());
        assertThrows(CustomerNotFoundException.class, () -> customerService.getOrdersByCustomerId(1L));
    }

    @Test()
    public void getOrdersFromCustomerTest_whenOrdersFetched_thenReturnOrderResponseList() {
        Customer c1 = Customer.builder()
                .id(1L)
                .name("Customer1").build();
        OrderRecords orderLine = OrderRecords.builder()
                .amount(new BigDecimal("12"))
                .book(Book.builder()
                        .id(1L)
                        .name("Book1").build())
                .id(1L).build();
        OrderHeader oh = OrderHeader.builder()
                .customer(c1)
                .id(1L)
                .totalAmount(new BigDecimal("100"))
                .lines(Collections.singletonList(orderLine)).build();
        Page<OrderHeader> page = new PageImpl<>(Collections.singletonList(oh));
        when(customerRepo.findById(1L))
                .thenReturn(Optional.of(c1));
        when(orderHeaderRepo.findAllByCustomer(eq(c1), any(Pageable.class)))
                .thenReturn(page);

        List<OrderResponse> list = customerService.getOrdersByCustomerId(1L);
        assertThat(list.get(0).getLines().get(0).getAmount()).isEqualTo(new BigDecimal("12"));
        assertThat(list.get(0).getLines().get(0).getId()).isEqualTo(1);

    }
}
