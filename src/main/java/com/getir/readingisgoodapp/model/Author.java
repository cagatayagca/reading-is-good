package com.getir.readingisgoodapp.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalDateTime;
import java.util.List;

@Entity(name = "author")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Author {
    @Id
    @SequenceGenerator(schema = "GETIR", name = "seqAuthorId", sequenceName = "seq_author_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqAuthorId")
    private Long id;

    @NotNull
    @Length(min = 3, max = 50, message = "name cannot be that long.")
    private String name;

    @OneToMany(mappedBy = "author")
    private List<Book> bookList;

    @CreationTimestamp
    @PastOrPresent(message = "invalid create DateTime")
    private LocalDateTime createdAt;

    @UpdateTimestamp
    @PastOrPresent(message = "invalid update DateTime")
    private LocalDateTime updatedAt;
}
