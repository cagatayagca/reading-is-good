package com.getir.readingisgoodapp.model.request;

import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderRequest {
    @NonNull
    private Long customerId;
    private List<BookRequest> bookRequestList;
}
