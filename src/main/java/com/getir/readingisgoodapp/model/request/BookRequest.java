package com.getir.readingisgoodapp.model.request;

import lombok.*;

import javax.validation.constraints.Positive;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BookRequest {
    @NonNull
    @Positive
    private Long bookId;
}
