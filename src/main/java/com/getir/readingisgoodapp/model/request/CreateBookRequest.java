package com.getir.readingisgoodapp.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateBookRequest {
    @NotEmpty
    @Length(min = 3, max = 100)
    private String name;
    @Positive
    private Long authorId;
    @Positive
    private Long quantity;
    @PositiveOrZero
    private BigDecimal amount;
}
