package com.getir.readingisgoodapp.model;

import com.getir.readingisgoodapp.model.request.CreateBookRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity(name = "book")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Book {
    @Id
    @SequenceGenerator(schema = "GETIR", name = "seqBookId", sequenceName = "seq_book_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqBookId")
    private Long id;

    @NotNull
    @Length(min = 3, max = 50, message = "name cannot be that long.")
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "author_id")
    private Author author;

    @OneToOne(mappedBy = "book", cascade = CascadeType.ALL)
    @Min(value = 0, message = "stock cannot be a negative number.")
    private BookStock bookStock;

    @DecimalMax("9999999.9")
    @DecimalMin("0.1")
    private BigDecimal amount;

    @CreationTimestamp
    @PastOrPresent(message = "invalid create DateTime")
    private LocalDateTime createdAt;

    @UpdateTimestamp
    @PastOrPresent(message = "invalid update DateTime")
    private LocalDateTime updatedAt;

    public static Book of(CreateBookRequest createBookRequest) {
        return Book.builder()
                .amount(createBookRequest.getAmount())
                .name(createBookRequest.getName())
                .build();
    }
}
