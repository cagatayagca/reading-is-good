package com.getir.readingisgoodapp.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity(name = "order_records")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderRecords {
    @Id
    @SequenceGenerator(schema = "GETIR", name = "seqOrderRecordId", sequenceName = "sequence_order_recs_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqOrderRecordId")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "header_id")
    private OrderHeader header;

    @ManyToOne
    @JoinColumn(name = "book_id")
    private Book book;
    @Positive
    private BigDecimal amount;

    @CreationTimestamp
    @PastOrPresent(message = "invalid create DateTime")
    private LocalDateTime createdAt;

    @UpdateTimestamp
    @PastOrPresent(message = "invalid update DateTime")
    private LocalDateTime updatedAt;
}
