package com.getir.readingisgoodapp.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Positive;
import java.time.LocalDateTime;

@Entity(name = "book_stock")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BookStock {
    @Id
    @SequenceGenerator(schema = "GETIR", name = "seqBookStockId", sequenceName = "seq_book_stock_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqBookStockId")
    private Long id;

    @Positive(message = "stock quantity cannot be a negative number.")
    private Long quantity;

    @OneToOne
    @JoinColumn(name = "book_id")
    private Book book;

    @CreationTimestamp
    @PastOrPresent(message = "invalid create DateTime")
    private LocalDateTime createdAt;

    @UpdateTimestamp
    @PastOrPresent(message = "invalid update DateTime")
    private LocalDateTime updatedAt;
}
