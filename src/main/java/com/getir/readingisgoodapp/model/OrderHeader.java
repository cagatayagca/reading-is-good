package com.getir.readingisgoodapp.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Entity(name = "order_headers")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderHeader {
    @Id
    @SequenceGenerator(schema = "GETIR", name = "seqOrderHeaderId", sequenceName = "seq_order_header_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqOrderHeaderId")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @OneToMany(mappedBy = "header", cascade = CascadeType.ALL)
    private List<OrderRecords> lines;

    @Positive(message = "total amount should be valid.")
    @DecimalMax("999999.9")
    private BigDecimal totalAmount;

    @CreationTimestamp
    @PastOrPresent(message = "invalid create DateTime")
    private LocalDateTime createdAt;

    @UpdateTimestamp
    @PastOrPresent(message = "invalid update DateTime")
    private LocalDateTime updatedAt;
}
