package com.getir.readingisgoodapp.model.response;

import com.getir.readingisgoodapp.utils.CommonUtils;
import com.getir.readingisgoodapp.utils.StatisticsConverterUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StatisticsForOneMonth {

    @NotEmpty
    private String month;
    @Length(min = 4, max = 4)
    @NotEmpty
    private String year;
    @PositiveOrZero
    private Long totalOrderCount;
    @PositiveOrZero
    private Long totalBookCount;
    @PositiveOrZero
    private BigDecimal totalPurchasedAmount;

    public static StatisticsForOneMonth of(StatisticsConverterUtil e) {
        return StatisticsForOneMonth.builder()
                .month(CommonUtils.getMonth(e.getMonth()))
                .year(CommonUtils.getYear(e.getMonth()))
                .totalBookCount(e.getTotalBookCount())
                .totalOrderCount(e.getTotalOrderCount())
                .totalPurchasedAmount(e.getTotalPurchasedAmount())
                .build();
    }
}
