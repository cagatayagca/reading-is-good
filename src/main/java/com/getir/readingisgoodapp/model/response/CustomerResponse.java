package com.getir.readingisgoodapp.model.response;

import com.getir.readingisgoodapp.model.Customer;
import lombok.*;

import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CustomerResponse {
    @NonNull
    private Long id;
    @NotEmpty
    private String name;

    public static CustomerResponse of(Customer customer) {
        return CustomerResponse.builder()
                .id(customer.getId())
                .name(customer.getName())
                .build();
    }
}
