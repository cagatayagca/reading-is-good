package com.getir.readingisgoodapp.model.response;

import com.getir.readingisgoodapp.model.Book;
import lombok.*;

import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BookResponse {
    @NonNull
    private Long id;
    @NotEmpty
    private String name;

    public static BookResponse of(Book book) {
        return BookResponse.builder()
                .id(book.getId())
                .name(book.getName())
                .build();
    }
}
