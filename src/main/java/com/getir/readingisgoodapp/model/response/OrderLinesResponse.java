package com.getir.readingisgoodapp.model.response;

import lombok.*;

import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderLinesResponse {
    @NonNull
    private Long id;
    private BookResponse book;
    @PositiveOrZero
    private BigDecimal amount;
}
