package com.getir.readingisgoodapp.model;

import com.getir.readingisgoodapp.model.request.CustomerRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalDateTime;
import java.util.List;

@Entity(name = "customer")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Customer {
    @Id
    @SequenceGenerator(schema = "GETIR", name = "seqCustomerId", sequenceName = "seq_customer_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqCustomerId")
    private Long id;
    @NotNull
    @Length(min = 3, max = 50, message = "name cannot be that long.")
    private String name;

    @OneToMany(mappedBy = "customer")
    private List<OrderHeader> orderHeaderList;

    @CreationTimestamp
    @PastOrPresent(message = "invalid create DateTime")
    private LocalDateTime createdAt;

    @UpdateTimestamp
    @PastOrPresent(message = "invalid update DateTime")
    private LocalDateTime updatedAt;

    public static Customer of(CustomerRequest customerRequest) {
        return Customer.builder()
                .name(customerRequest.getName())
                .createdAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .build();
    }
}
