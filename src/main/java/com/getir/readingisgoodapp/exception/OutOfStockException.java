package com.getir.readingisgoodapp.exception;

public class OutOfStockException extends BusinessException {
    private static final String errorCode = "1004";

    public OutOfStockException(String errorMessage) {
        super(errorCode, errorMessage);
    }
}
