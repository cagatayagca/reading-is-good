package com.getir.readingisgoodapp.exception;

public class OrderNotFoundException extends BusinessException {
    private static final String errorCode = "1003";
    private static final String errorMessage = "Order Is Not Found";

    public OrderNotFoundException() {
        super(errorCode, errorMessage);
    }
}