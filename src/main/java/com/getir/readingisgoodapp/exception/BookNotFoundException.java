package com.getir.readingisgoodapp.exception;

public class BookNotFoundException extends BusinessException {
    private static final String errorCode = "1002";
    private static final String errorMessage = "Book Is Not Found";

    public BookNotFoundException() {
        super(errorCode, errorMessage);
    }
}