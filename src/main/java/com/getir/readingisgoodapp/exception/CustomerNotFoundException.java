package com.getir.readingisgoodapp.exception;

public class CustomerNotFoundException extends BusinessException {
    private static final String errorCode = "1001";
    private static final String errorMessage = "Customer Is Not Found";

    public CustomerNotFoundException() {
        super(errorCode, errorMessage);
    }
}
