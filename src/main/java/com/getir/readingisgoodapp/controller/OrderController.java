package com.getir.readingisgoodapp.controller;

import com.getir.readingisgoodapp.model.request.OrderRequest;
import com.getir.readingisgoodapp.model.response.OrderResponse;
import com.getir.readingisgoodapp.service.OrderService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/orders")
public class OrderController {
    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping
    public ResponseEntity<OrderResponse> createOrder(@Valid @RequestBody OrderRequest orderRequest) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(orderService.createOrder(orderRequest));
    }

    @GetMapping("/{id}")
    public ResponseEntity<OrderResponse> getOrderById(@Valid @PathVariable Long id) {
        return ResponseEntity.ok(orderService.getById(id));
    }
}
