package com.getir.readingisgoodapp.controller;

import com.getir.readingisgoodapp.model.request.CustomerRequest;
import com.getir.readingisgoodapp.model.response.CustomerResponse;
import com.getir.readingisgoodapp.model.response.OrderResponse;
import com.getir.readingisgoodapp.service.CustomerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/customers")
public class CustomerController {
    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;

    }

    @PostMapping
    public ResponseEntity<CustomerResponse> createCustomer(@Valid @RequestBody CustomerRequest customerRequest) {
        return ResponseEntity.status(HttpStatus.CREATED).body(customerService.createCustomer(customerRequest));
    }

    @GetMapping("/{id}/orders")
    public ResponseEntity<List<OrderResponse>> getAllByCustomerId(@Valid @PathVariable Long id) {
        return ResponseEntity.ok(customerService.getOrdersByCustomerId(id));
    }
}
