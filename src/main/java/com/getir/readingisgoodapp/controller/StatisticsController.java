package com.getir.readingisgoodapp.controller;

import com.getir.readingisgoodapp.model.response.MonthlyReportResponse;
import com.getir.readingisgoodapp.service.StatisticsService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/statistics")
public class StatisticsController {

    private final StatisticsService service;

    public StatisticsController(StatisticsService statisticsService) {
        this.service = statisticsService;
    }

    @GetMapping("/{customerId}/monthly-report")
    public ResponseEntity<MonthlyReportResponse> getMonthlyStatistics(@Valid @PathVariable Long customerId) {
        return ResponseEntity.ok(service.getMonthlyReport(customerId));
    }
}
