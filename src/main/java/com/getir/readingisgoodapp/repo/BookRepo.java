package com.getir.readingisgoodapp.repo;

import com.getir.readingisgoodapp.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepo extends JpaRepository<Book, Long> {
}
