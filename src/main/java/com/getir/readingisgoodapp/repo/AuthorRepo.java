package com.getir.readingisgoodapp.repo;

import com.getir.readingisgoodapp.model.Author;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorRepo extends JpaRepository<Author, Long> {
}
