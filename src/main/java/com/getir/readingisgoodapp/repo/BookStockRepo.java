package com.getir.readingisgoodapp.repo;

import com.getir.readingisgoodapp.model.Book;
import com.getir.readingisgoodapp.model.BookStock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;

import javax.persistence.LockModeType;

public interface BookStockRepo extends JpaRepository<BookStock, Long> {

    // Can be Optimistic lock in order to increase performance
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    BookStock findByBook(Book book);
}
