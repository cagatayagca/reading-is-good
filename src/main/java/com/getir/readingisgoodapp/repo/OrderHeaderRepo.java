package com.getir.readingisgoodapp.repo;

import com.getir.readingisgoodapp.model.Customer;
import com.getir.readingisgoodapp.model.OrderHeader;
import com.getir.readingisgoodapp.utils.StatisticsConverterUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface OrderHeaderRepo extends JpaRepository<OrderHeader, Long> {
    @Query(
            value = "SELECT to_char(a.created_at, 'MMYYYY') month, sum(amount) totalPurchasedAmount, " +
                    "count(b.id) totalBookCount, count(distinct a.id) totalOrderCount\n" +
                    "FROM GETIR.ORDER_HEADERS a, GETIR.order_records b\n" +
                    "where a.id = b. header_id\n" +
                    "and a.customer_id = ?1\n" +
                    "group by to_char(a.created_at, 'MMYYYY')",
            nativeQuery = true)
    List<StatisticsConverterUtil> generateMonthlyReport(Long customerId);

    Page<OrderHeader> findAllByCustomer(Customer customer, Pageable pageable);
}
