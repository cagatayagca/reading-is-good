package com.getir.readingisgoodapp.repo;

import com.getir.readingisgoodapp.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepo extends JpaRepository<Customer, Long> {
}
