package com.getir.readingisgoodapp.service.impl;

import com.getir.readingisgoodapp.model.response.MonthlyReportResponse;
import com.getir.readingisgoodapp.model.response.StatisticsForOneMonth;
import com.getir.readingisgoodapp.repo.OrderHeaderRepo;
import com.getir.readingisgoodapp.service.StatisticsService;
import com.getir.readingisgoodapp.utils.StatisticsConverterUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class StatisticsServiceImpl implements StatisticsService {

    private final OrderHeaderRepo orderHeaderRepo;

    @Override
    public MonthlyReportResponse getMonthlyReport(Long customerId) {
        log.info("---- Requesting stats for customer: {} ----", customerId);
        List<StatisticsConverterUtil> list = orderHeaderRepo.generateMonthlyReport(customerId);
        List<StatisticsForOneMonth> statisticForOneMonth = list.stream()
                .map(StatisticsForOneMonth::of).collect(Collectors.toList());
        MonthlyReportResponse response = MonthlyReportResponse.builder()
                .statisticForOneMonthList(statisticForOneMonth)
                .build();
        log.info("---- Report is ready for customer:{}, report size:{}",
                customerId, response.getStatisticForOneMonthList().size());
        return response;
    }
}
