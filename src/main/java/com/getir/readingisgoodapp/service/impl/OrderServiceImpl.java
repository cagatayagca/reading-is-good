package com.getir.readingisgoodapp.service.impl;

import com.getir.readingisgoodapp.exception.BookNotFoundException;
import com.getir.readingisgoodapp.exception.OrderNotFoundException;
import com.getir.readingisgoodapp.exception.OutOfStockException;
import com.getir.readingisgoodapp.model.*;
import com.getir.readingisgoodapp.model.request.BookRequest;
import com.getir.readingisgoodapp.model.request.OrderRequest;
import com.getir.readingisgoodapp.model.response.OrderResponse;
import com.getir.readingisgoodapp.repo.BookRepo;
import com.getir.readingisgoodapp.repo.BookStockRepo;
import com.getir.readingisgoodapp.repo.OrderHeaderRepo;
import com.getir.readingisgoodapp.service.CustomerService;
import com.getir.readingisgoodapp.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderHeaderRepo orderHeaderRepo;
    private final CustomerService customerService;
    private final BookRepo bookRepo;
    private final BookStockRepo bookStockRepo;

    @Override
    @Transactional(rollbackFor = RuntimeException.class, isolation = Isolation.READ_COMMITTED)
    public OrderResponse createOrder(OrderRequest orderRequest) {
        log.info("---- Creating new order for customer: {}, numberOfBooks: {} ----",
                orderRequest.getCustomerId(), orderRequest.getBookRequestList().size());
        Customer customer = customerService.findById(orderRequest.getCustomerId());

        List<OrderRecords> orderLineList = new ArrayList<>();
        BigDecimal totalAmount = new BigDecimal(0);
        OrderHeader orderHeader = OrderHeader.builder()
                .customer(customer)
                .lines(orderLineList)
                .createdAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .build();
        for (BookRequest bt : orderRequest.getBookRequestList()) {
            Book book = bookRepo.findById(bt.getBookId()).orElseThrow(BookNotFoundException::new);
            getAndDeductBookStock(book);
            OrderRecords ol = OrderRecords.builder()
                    .book(book)
                    .header(orderHeader)
                    .amount(book.getAmount())
                    .createdAt(LocalDateTime.now())
                    .updatedAt(LocalDateTime.now())
                    .build();
            totalAmount = totalAmount.add(book.getAmount());
            orderLineList.add(ol);
            log.debug("---- Order is added to customer: {}, book: {} ----", orderRequest.getCustomerId(), book.getId());
        }
        orderHeader.setTotalAmount(totalAmount);
        log.debug("---- Order is ready for customer: {}, totalAmount: {} ----",
                orderRequest.getCustomerId(), totalAmount);
        OrderHeader oh = orderHeaderRepo.save(orderHeader);
        log.info("---- Order response is ready, ID:{} ----", orderHeader.getId());
        return OrderResponse.of(oh);
    }

    @Override
    public void getAndDeductBookStock(Book book) {
        BookStock bookStock = bookStockRepo.findByBook(book);
        if (bookStock.getQuantity() < 1) {
            throw new OutOfStockException(book.getName() + " is out of stock!");
        }
        bookStock.setQuantity(bookStock.getQuantity() - 1);
        bookStockRepo.save(bookStock);
    }

    @Override
    public OrderResponse getById(Long id) {
        OrderHeader oh = orderHeaderRepo.findById(id).orElseThrow(OrderNotFoundException::new);
        return OrderResponse.of(oh);
    }
}
