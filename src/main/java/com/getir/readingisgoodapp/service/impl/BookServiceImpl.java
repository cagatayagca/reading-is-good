package com.getir.readingisgoodapp.service.impl;

import com.getir.readingisgoodapp.exception.BookNotFoundException;
import com.getir.readingisgoodapp.model.Author;
import com.getir.readingisgoodapp.model.Book;
import com.getir.readingisgoodapp.model.BookStock;
import com.getir.readingisgoodapp.model.request.CreateBookRequest;
import com.getir.readingisgoodapp.model.response.BookResponse;
import com.getir.readingisgoodapp.repo.AuthorRepo;
import com.getir.readingisgoodapp.repo.BookRepo;
import com.getir.readingisgoodapp.service.BookService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

@Slf4j
@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {

    private final BookRepo bookRepo;
    private final AuthorRepo authorRepo;

    @Override
    @Transactional
    public BookResponse createBook(CreateBookRequest createBookRequest) {
        log.info("---- Creating new book for: {} ----", createBookRequest.getName());
        Author author = authorRepo.findById(createBookRequest.getAuthorId())
                .orElseThrow(BookNotFoundException::new);
        Book book = Book.of(createBookRequest);
        book.setAuthor(author);
        BookStock bookStock = BookStock.builder()
                .book(book)
                .quantity(createBookRequest.getQuantity())
                .createdAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .build();
        book.setBookStock(bookStock);
        BookResponse newBook = BookResponse.of(bookRepo.save(book));
        log.info("---- New book is created named: {} by ID: {} ----", newBook.getName(), newBook.getId());
        return newBook;
    }
}
