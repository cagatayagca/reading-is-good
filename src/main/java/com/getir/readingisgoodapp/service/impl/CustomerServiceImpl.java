package com.getir.readingisgoodapp.service.impl;

import com.getir.readingisgoodapp.exception.CustomerNotFoundException;
import com.getir.readingisgoodapp.model.Customer;
import com.getir.readingisgoodapp.model.request.CustomerRequest;
import com.getir.readingisgoodapp.model.response.CustomerResponse;
import com.getir.readingisgoodapp.model.response.OrderResponse;
import com.getir.readingisgoodapp.repo.CustomerRepo;
import com.getir.readingisgoodapp.repo.OrderHeaderRepo;
import com.getir.readingisgoodapp.service.CustomerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepo customerRepo;
    private final OrderHeaderRepo orderHeaderRepo;

    @Override
    @Transactional
    public CustomerResponse createCustomer(CustomerRequest customerRequest) {
        log.info("---- Creating new Customer with name: {} ----", customerRequest.getName());
        Customer customer = customerRepo.save(Customer.of(customerRequest));
        CustomerResponse newCustomer = CustomerResponse.of(customer);
        log.info("---- New customer is ready. ID:{} ----", newCustomer.getId());
        return newCustomer;
    }

    @Override
    public List<OrderResponse> getOrdersByCustomerId(Long id) {
        log.info("---- Requesting orders for customer: {} ----", id);
        Customer customer = findById(id);
        List<OrderResponse> orderResponseList = new ArrayList<>();
        orderHeaderRepo.findAllByCustomer(customer, Pageable.ofSize(10)).forEach(order ->
                orderResponseList.add(OrderResponse.of(order)));
        log.info("---- Orders are ready, returning. ----");
        return orderResponseList;
    }

    @Override
    public Customer findById(Long id) {
        return customerRepo.findById(id).orElseThrow(CustomerNotFoundException::new);
    }
}
