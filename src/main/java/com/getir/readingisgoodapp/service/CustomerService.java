package com.getir.readingisgoodapp.service;

import com.getir.readingisgoodapp.model.Customer;
import com.getir.readingisgoodapp.model.request.CustomerRequest;
import com.getir.readingisgoodapp.model.response.CustomerResponse;
import com.getir.readingisgoodapp.model.response.OrderResponse;

import java.util.List;

public interface CustomerService {

    Customer findById(Long id);

    List<OrderResponse> getOrdersByCustomerId(Long id);

    CustomerResponse createCustomer(CustomerRequest customerRequest);
}
