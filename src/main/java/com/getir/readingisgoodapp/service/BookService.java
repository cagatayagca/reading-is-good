package com.getir.readingisgoodapp.service;

import com.getir.readingisgoodapp.model.request.CreateBookRequest;
import com.getir.readingisgoodapp.model.response.BookResponse;

public interface BookService {

    BookResponse createBook(CreateBookRequest createBookRequest);
}
