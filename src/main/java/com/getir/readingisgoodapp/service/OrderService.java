package com.getir.readingisgoodapp.service;

import com.getir.readingisgoodapp.model.Book;
import com.getir.readingisgoodapp.model.request.OrderRequest;
import com.getir.readingisgoodapp.model.response.OrderResponse;

public interface OrderService {

    void getAndDeductBookStock(Book book);

    OrderResponse getById(Long id);

    OrderResponse createOrder(OrderRequest orderRequest);
}
