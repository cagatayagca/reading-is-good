package com.getir.readingisgoodapp.service;

import com.getir.readingisgoodapp.model.response.MonthlyReportResponse;

public interface StatisticsService {

    MonthlyReportResponse getMonthlyReport(Long customerId);
}
