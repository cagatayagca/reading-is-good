package com.getir.readingisgoodapp.utils;

import java.math.BigDecimal;

public interface StatisticsConverterUtil {
    String getMonth();

    Long getTotalOrderCount();

    Long getTotalBookCount();

    BigDecimal getTotalPurchasedAmount();
}
