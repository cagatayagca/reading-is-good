FROM openjdk:11-slim
ARG JAR_FILE=target/*.jar
EXPOSE 8080
COPY ${JAR_FILE} /project/reading-is-good-app.jar
ENTRYPOINT ["java","-jar","project/reading-is-good-app.jar"]